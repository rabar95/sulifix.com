<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $table='prices';

    public function post()
    {
        return $this->belongsTo(\App\Post::class);
    }
}
