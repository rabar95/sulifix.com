<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function prices()
    {
        return $this->hasMany(\App\Price::class);
    }
}
