<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
// use App\Page;
use App\Site;

class postController extends Controller
{
    public function index()
    {
        $posts=Post::all();
        $sites=Site::all();
        return view('pages.index', compact('posts', 'sites'));
    }

    public function show(Post $post)
    {
        return view('pages.priceshow', compact('post'));
    }

    public function about()
    {
        $sites=Site::all();
        return view('pages.about', compact('sites'));
    }
}
