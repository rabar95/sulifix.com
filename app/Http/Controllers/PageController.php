<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;

class PageController extends Controller
{
    public function about()
    {
        $pages=Page::all();
        return view('pages.index', compact('Pages'));
    }
}
