	
	<!-- Contact -->
	<div id="contact" class="section md-padding">

		<!-- Container -->
		<div class="container">

			<!-- Row -->
			<div class="row">

				<!-- Section-header -->
				<div class="section-header text-center">
					<h2 class="title">پەیوەندی کردن</h2>
				</div>
				<!-- /Section-header -->

				<!-- contact -->
				<div class="col-sm-6">
					<div class="contact">
						<i class="fa fa-phone"></i>
						<h3>پەیوەندی</h3>
						<p>751-190-5738</p>
					</div>
				</div>
				<!-- /contact -->

				<!-- contact -->
				<div class="col-sm-6">
					<div class="contact">
						<i class="fa fa-envelope"></i>
						<h3>ئیمەیڵ</h3>
						<p>sulifix0@gmail.com</p>
					</div>
				</div>
				<!-- /contact -->
				
				<div class="cal-lg-12" >
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6490.109568440494!2d45.331617102829796!3d35.57703960102891!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x400031c225d39c1d%3A0xc5e7d07898be1e6b!2sSulifix.com!5e0!3m2!1sen!2siq!4v1552824901397"
						width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>		
				</div>


			
				
			</div>
			<!-- /Row -->

		</div>
		<!-- /Container -->
		

	</div>
	<!-- /Contact -->
