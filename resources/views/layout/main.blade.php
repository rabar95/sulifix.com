<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

	<title>SuliFix</title>

	<!-- Google font -->
<link href="{{ url('https://fonts.googleapis.com/css?family=Montserrat:400,700%7CVarela+Round')}}" rel="stylesheet">

	<!-- Bootstrap -->
<link type="text/css" rel="stylesheet" href="{{ url('css/bootstrap.min.css')}}" />

	<!-- Owl Carousel -->
<link type="text/css" rel="stylesheet" href="{{ url('css/owl.carousel.css')}}" />
<link type="text/css" rel="stylesheet" href="{{url('css/owl.theme.default.css')}}" />

	<!-- Magnific Popup -->
	<link type="text/css" rel="stylesheet" href="{{ url('css/magnific-popup.css')}}" />

	<!-- Font Awesome Icon -->
<link rel="stylesheet" href="{{ url('css/font-awesome.min.css')}}">

	<!-- Custom stlylesheet -->
<link type="text/css" rel="stylesheet" href="{{ url('css/style.css')}}" />
<link rel="stylesheet" href="{{url('css/parsley.css')}}">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
</head>

<body >
	<!-- Header -->
	<header id="home" dir="rtl">
		<!-- Background Image -->
		<div class="bg-img" style="background-image: url('./storage/bg/background1.jpg');">
			<div class="overlay"></div>
		</div>
		<!-- /Background Image -->

	
		<!-- Nav -->
		<nav id="nav" class="navbar nav-transparent">
				
			<div class="container">
					
				<div class="navbar-header">
					<!-- Logo -->
					<div class="navbar-brand">
						<a href="index.html">
							<a href="{{url('/')}}"><img class="logo" src="{{url('img/logo.png')}}" alt="logo"></a>
							<a href="{{url('/')}}"><img class="logo-alt" src="{{url('img/logo.png')}}" alt="logo"></a>
						</a>
					</div>
					<!-- /Logo -->

					<!-- Collapse nav button -->
					<div class="nav-collapse">
						<span></span>
						
					</div>
					<!-- /Collapse nav button -->
				</div>
			
				
				<!--  Main navigation  -->
				<ul class="main-nav nav navbar-nav navbar-right">
					<li><a href="#home">Home</a></li>
					<li><a href="#about">Category</a></li>
					<li><a href="#features">? Why Chose Us</a></li>
					<li><a href="#contact">Contact Us</a></li>
				</ul>
				<!-- /Main navigation -->

			</div>
		</nav>
        <!-- /Nav -->
        

        @yield('content')


	@include('layout.mainfooter')

	<!-- Back to top -->
	<div id="back-to-top"></div>
	<!-- /Back to top -->

	<!-- Preloader -->
	<div id="preloader">
		<div class="preloader">
			<span></span>
			<span></span>
			<span></span>
			<span></span>
		</div>
	</div>
	<!-- /Preloader -->

	<!-- jQuery Plugins -->
	
	<script type="text/javascript" src="{{ url('js/jquery.min.js')}}"></script>
	<script type="text/javascript" src="{{ url('js/bootstrap.min.js')}}"></script>
	<script type="text/javascript" src="{{ url('js/owl.carousel.min.js')}}"></script>
	<script type="text/javascript" src="{{ url('js/jquery.magnific-popup.js')}}"></script>
	<script type="text/javascript" src="{{ url('js/main.js')}}"></script>
	<script src="{{url('js/parsley.min.js')}}"></script>

</body>

</html>
