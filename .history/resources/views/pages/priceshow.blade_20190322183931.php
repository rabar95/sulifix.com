@extends('layout.main')

@section('content')

        <div class="home-wrapper allbg" >
                <div class="container">
                    <div  dir="rtl" >
                        <div class="titlep">
                             <h2>{{$post->title}}</h2>
                        </div>
                             
                        {{-- <h4 class="white-text">{{$post->body}}</h4> --}}
                    </div>  
        
                      @foreach ($post->prices as $price)
                        
                      {{-- <div class="cell" data-title="Full Name">
                            {{$price->price_no}}
                        </div> --}}
                          <div class="cell" data-title="Age">
                              <h4>{{$price->information}}</h4>   
                          </div>

                          <div class="row">
                            <div class="col-md-3">
                                <div class="pngh4">
                                    <h4>کێشەی ئامێر؟</h4>
                                </div>
                                
                                <div class="">
                                        <img class="img-fluid" src='{{ asset("storage/$price->png1") }}' >
                                </div>
                                
                            </div>
                            <div class="col-md-3">
                                <div class="pngh4">
                                    <h4>کامێرەی چاودێری</h4>
                                </div>
                                    
                                <div>
                                    <img class="img-fluid" src='{{ asset("storage/$price->png2") }}' >
                                </div>
                                
                            </div>
                          </div>
                      @endforeach
              </div>  
                @endsection
            </div>
        

