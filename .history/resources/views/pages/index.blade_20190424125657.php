@extends('layout.main')

@section('content')
	

		<!-- home wrapper -->
		<div class="home-wrapper ">
			<div class="container">
				<div class="row">

					<!-- home content -->
					<div class="col-md-10 col-md-offset-1">
						<div class="home-content">
							<h2 class="white-text">We care your butifull house</h2>
							<h2 class="white-text"> Repair Relax</h2>
							</p>
					
						</div>
					</div>
					<!-- /home content -->

				</div>
			</div>
		</div>
		<!-- /home wrapper -->

	</header>
	<!-- /Header -->

	
       {{-- post --}}
		@include('pages.post')

			
		{{-- About us --}}
		@include('pages.about')
			

		{{-- contact --}}
		@include('pages.sendorder')

	@endsection