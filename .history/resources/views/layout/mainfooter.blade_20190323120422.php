@yield('content')
<!-- Footer -->
<footer id="footer" class="sm-padding bg-dark">

    <!-- Container -->
    <div class="container">

        <!-- Row -->
        <div class="row">

            <div class="col-md-12">

                <!-- footer logo -->
                <div class="footer-logo">
                <a href="{{url('/')}}"><img src="{{url('img/logo-alt.png')}}" alt="logo"></a>
                </div>
                <!-- /footer logo -->

                <!-- footer follow -->
                <ul class="footer-follow">
                    <li><a href="https://www.facebook.com/sulifix/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://mail.google.com/mail/u/0/?tab=wm#inbox?compose=GTvVlcSBmzjrTtxqSdkFJcBvNNnXcRzPxlDxNTRFmrBCrXTPRmGSQtSTjQvDxttWmgzqQwRDjZCpq" target="_blank"><i class="fa fa-envelope"></i></a></li>
                    <li><a href="#"><i class="fa fa-phone"></i></a></li>
                    <li><a href="https://www.instagram.com/sulifix0/"><i class="fa fa-instagram"></i></a></li>
        
                </ul>
                <!-- /footer follow -->

                <!-- footer copyright -->
                <div class="footer-copyright">
                    <p>Copyright © 2018. Developed by <a href="https://www.facebook.com/rabar95" target="_blank">Rabar Sdiq</a></p>
                </div>
                <!-- /footer copyright -->

            </div>

        </div>
        <!-- /Row -->

    </div>
    <!-- /Container -->

</footer>
<!-- /Footer -->